@echo off
set csc=%windir%\Microsoft.NET\Framework\v2.0.50727\csc.exe
if NOT EXIST "%csc%" (
	echo I can't find the .NET 2.0 framework at "%csc%".
	EXIT /b
)


%csc% /out:bin\xortag.exe Program.cs /reference:lib\Newtonsoft.Json.dll 
copy lib\Newtonsoft.Json.dll bin

bin\xortag.exe
