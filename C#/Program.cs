﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

namespace XorTagSample
{
    class Program
    {
        private static void Move()
        {
            while (moving)
            {
                switch (rand.Next(4))
                {
                    case 0:
                        MoveUp();
                        break;
                    case 1:
                        MoveRight();
                        break;
                    case 2:
                        MoveDown();
                        break;
                    case 3:
                        MoveLeft();
                        break;
                }
            }
        }

        #region Helper Methods
        private static void MoveUp()
        {
            world = SendMoveCommand("moveUp");
        }

        private static void MoveDown()
        {
            world = SendMoveCommand("moveDown");
        }

        private static void MoveRight()
        {
            world = SendMoveCommand("moveRight");
        }

        private static void MoveLeft()
        {
            world = SendMoveCommand("moveLeft");
        }

        private static void Look()
        {
            Thread.Sleep(1000); //Requests more frequent than once per second will fail.
            CheckForRegistration();
            world = JsonConvert.DeserializeObject<ResponseViewModel>(_client.DownloadString(Url + "look/" + world.Id));
        }

        private static void Register()
        {
            world = JsonConvert.DeserializeObject<ResponseViewModel>(_client.DownloadString(Url + "register"));
            Console.WriteLine("You have successfully registered!");
            Console.WriteLine("Your player name is {0} and your id is {1}", world.Name, world.Id);
        }

        private static ResponseViewModel SendMoveCommand(string moveCommand)
        {
            Thread.Sleep(1000); //Requests more frequent than once per second will fail.
            CheckForRegistration();
            return JsonConvert.DeserializeObject<ResponseViewModel>(_client.DownloadString(Url + moveCommand + "/" + world.Id));
        }

        private static void CheckForRegistration()
        {
            if (world == null) throw new InvalidOperationException("You have to register as a player before you can move");
        }

        static void Main(string[] args)
        {
            _client = new WebClient();

            Register();

            StartMoving();

            Console.Write("Press any key to exit");
            Console.ReadKey();
            moving = false;
        }

        private static void StartMoving()
        {
            Thread movementThread = new Thread(Move);
            movementThread.IsBackground = true;
            movementThread.Name = "XorTag movement thread";
            moving = true;
            movementThread.Start();
        }

        #endregion

        private const string Url = "http://xortag.apphb.com/";
        private static ResponseViewModel world;
        private static WebClient _client;
        private static bool moving;
        private static Random rand = new Random();

    }

    public class ResponseViewModel
    {
        public string Id;
        public string Name;
        public bool IsIt;
        public int X;
        public int Y;
        public int MapWidth;
        public int MapHeight;
        public IEnumerable<PlayerViewModel> Players;
    }

    public class PlayerViewModel
    {
        public int X;
        public int Y;
        public bool IsIt;
    }
}
